<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Home
Route::get('/', 'WelcomeController@index');

Route::get('/home',function()
{
	return view('home');
});



//Post

Route::get('/post/new',function()
{
	return view('/post/new');
});

Route::get('/post/list',function()
{
	return view('/post/list');
});

Route::get('/post/delete/image/{id}', 'Post\PostController@delete_image');

Route::get('/post/edit/{id}', 'Post\PostController@editpost');

Route::post('post/save',array('uses'=>'Post\PostController@save'));

Route::post('post/update',array('uses'=>'Post\PostController@update'));

Route::post('post/update/img',array('uses'=>'Post\PostController@update_img'));

Route::get('/post/delete/{id}', 'Post\PostController@deletepost');


//Login
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	
]);


//Admin- User Account

Route::get('/admin/users',function()
{
	return view('/admin/users');
});

Route::post('user/edit',array('uses'=>'Admin\AdminController@update'));

Route::get('/admin/user/edit/{id}', 'Admin\AdminController@edit');
Route::get('/admin/user/delete/{id}', 'Admin\AdminController@delete');

//Admin-Posts
Route::get('/admin/user/posts',function()
{
	return view('/admin/posts');
});