<?php namespace App\Http\Controllers\Post;


use App\Http\Controllers\Post;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;

class PostController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */


	public function richTextBox($fieldname, $value = “”) {

   
   $CKEditor = new CKEditor();
   $config[‘toolbar’] = array(
    array( ‘Bold’, ‘Italic’, ‘Underline’, ‘Strike’),
    array(‘JustifyLeft’, ‘JustifyCenter’, ‘JustifyRight’, ‘JustifyBlock’),
    array(‘Format’),
    array( ‘Font’, ‘FontSize’, ‘FontColor’, ‘TextColor’),
    array(‘Cut’, ‘Copy’, ‘Paste’, ‘PasteText’, ‘PasteFromWord’ ),
    array(‘Image’, ‘Table’, ‘NumberedList’, ‘BulletedList’, ‘-‘, ‘Outdent’, ‘Indent’, ‘Blockquote’),
    array(‘Subscript’,’Superscript’),
    array( ‘Link’, ‘Unlink’ ),
    array(‘Source’)
   );

   $CKEditor->basePath = '/ckeditor/';
   $CKEditor->config[‘width’] = 975;
   $CKEditor->config[‘height’] = 400;
   
   $CKEditor->editor($fieldname, $value, $config);
}


	public function index()
	{
		return View('Teacher/index');
	}

	public function editpost($id)
	{
		return View('post/edit')->with('id',$id);
	}

	public function image($id)
	{
		return View('post/update_img')->with('id',$id);
	}


	public function deletepost($id)
	{
		DB::table('posts')->where('id',$id)->delete();

		return Redirect::to('/post/list');
	}

	public function delete_image($id)
	{
		$data=array('images'=>'');
			$check=0;
			$check=DB::table('posts')->where ('id',$id)->update($data);
			if($check>0)
			{
				return View('post/edit')->with('id',$id);
			}
			else
			{
				return Redirect::to ('/post/new');
			}
	}


	

	public function save()
	{ 


		$validation=array(
			'title'=>'required',
			'content'=>'required',
			);

		$vl=validator::make(Input::all(),$validation);
		if ($vl->fails())
		{
			return Redirect::to ('post/new')->withErrors($vl);

		}
		else
		{

			$destinationPath = 'uploads';
			$extension = Input::file('images')->getClientOriginalExtension(); // getting image extension
      		$fileName = rand(11111,99999).'.'.$extension; // renameing image
      			//var_dump($fileName);exit();
      		Input::file('images')->move($destinationPath, $fileName); // uploading file to given path

			$postnew=Input::all();
		
			$data=array('title'=>$postnew['title'],
						
						'content'=>$postnew['content'],
						'images'=>$fileName,
						'created_at'=>$postnew['created_at'],
						'published_on'=>$postnew['published_on'],
						'user_id'=>$postnew['user_id']);
			$check=0;
			$check=DB::table('posts')->insert($data);
			if($check>0)
			{
				return Redirect('/post/list');
			}
			else
			{
				return Redirect::to ('/post/new');
			}
		}
	}


	public function update()
	{ 
		$validation=array(
			'title'=>'required',
			'content'=>'required',
			);

		$vl=validator::make(Input::all(),$validation);
		if ($vl->fails())
		{
			return Redirect::to ('post/edit')->withErrors($vl);

		}
		else
		{

			

			$postnew=Input::all();
		
			$data=array('title'=>$postnew['title'],
					
						'content'=>$postnew['content'],
						'created_at'=>$postnew['created_at'],
						'published_on'=>$postnew['published_on'],
						'user_id'=>$postnew['user_id']);
			$check=0;
			$check=DB::table('posts')->where ('id',$postnew['id'])->update($data);
			if($check>0)
			{
				return Redirect('/post/list');
			}
			else
			{
				return Redirect::to ('/post/edit');
			}
		}
	}




	public function update_img()
	{ 
			$destinationPath = 'uploads';
			$extension = Input::file('images')->getClientOriginalExtension(); // getting image extension
      		$fileName2 = rand(11111,99999).'.'.$extension; // renameing image
      			
      		Input::file('images')->move($destinationPath, $fileName2); // uploading file to given path

			$postimg=Input::all();
			$id=$postimg['id'];
		
			$data=array('images'=>$fileName2,
						'published_on'=>$postimg['published_on']);
			$check=0;
			$check=DB::table('posts')->where ('id',$postimg['id'])->update($data);
			if($check>0)
			{
				return View('post/edit')->with('id',$id);
			}
			else
			{
				return Redirect::to ('/post/new');
			}
			
	}
	





}
