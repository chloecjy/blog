<?php namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;

class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */


	

	public function edit($id)
	{
		return View('admin/edit_user')->with('id',$id);
	}

	

	public function delete($id)
	{
		DB::table('users')->where('id',$id)->delete();

		return Redirect::to('/admin/users');
	}

	

	public function update()
	{ 		

		$edituser=Input::all();

		 $row=DB::table('users')->where('id',$edituser['id'])->first();

		 
         
          $default_pswd=$row->password;
          $pswd=$edituser['password'];

			

			if ($pswd === $default_pswd){


		
			$data=array('name'=>$edituser['name'],
					
						'email'=>$edituser['email'],

						'password'=>$default_pswd);
		}


		else
		{

			$data=array('name'=>$edituser['name'],
					
						'email'=>$edituser['email'],

						'password'=>bcrypt($pswd));
		}


		
			$check=0;
			$check=DB::table('users')->where ('id',$edituser['id'])->update($data);
			if($check>0)
			{
				return Redirect('/admin/users');
			}
			else
			{
				return View('admin/user/edit')->with('id',$id);
			}
		
	}




	


}
