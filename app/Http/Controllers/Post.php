<?php namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;

class Post extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	

		public function save()
	{ 
		$validation=array(
			'title'=>'required',
			'content'=>'required',
			);

		$vl=validator::make(Input::all(),$validation);
		if ($vl->fails())
		{
			return Redirect::to ('/post/new')->withErrors($vl);

		}
		else
		{
			$postnew=Input::all();
			$data=array('title'=>$postnew['title'],
						'content'=>$postnew['content']);
						
			$check=0;
			$check=DB::table('blog')->insert($data);
			if($check>0)
			{
				return Redirect('/');
			}
			else
			{
				return Redirect::to ('/post/new');
			}
		}
	}

}
