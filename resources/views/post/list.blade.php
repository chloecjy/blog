@extends('app')
<head>
<script type="text/javascript" src="/ckeditor/ckeditor/ckeditor.js"></script>
</head>

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<ul class="nav nav-tabs">
  
    <li role="presentation" class="dropdown">
    <a class="dropdown-toggle" href="{{ url('/post/new') }}" role="button" aria-expanded="false" >
      New Post 
    </a>
  </li>

  <li role="presentation" class="dropdown">
    <a class="dropdown-toggle" href="{{ url('/post/list') }}" role="button" aria-expanded="false" >
      Posts 
    </a>
  </li>
   
  <li role="presentation" class="dropdown">
    <a class="dropdown-toggle" href="{{ url('/home') }}" role="button" aria-expanded="false" >
      View Blog
    </a>
  </li>

</ul>


				<div class="panel-body">
					<h2>Posts</h2>
					<hr>
					<br>

					<table class="table">
     
      <thead>
        <tr>
         
          <th>Title</th>
          <th>Created On</th>
          <th>Publish On</th>
          <th></th>
        </tr>
      </thead>
      <tbody>

        <?php

        $id=Auth::user()->id;

        $Post=DB::table('posts')->where('user_id',$id)->get();

    //$Post=DB::table('posts')->get();
    foreach($Post as $row){
    ?>

        <tr>
          
          <td><?php echo $row->title ?></td>
          <td><?php echo $row->created_at ?></td>
          <td><?php echo $row->published_on ?></td>
          <td><a href="<?= URL::to('post/edit',array($row->id)) ?>"> Edit</a> | <a href="<?= URL::to('post/delete',array($row->id)) ?>">Delete</td>
        </tr>
      <?php } ?>
        
      </tbody>
    </table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
