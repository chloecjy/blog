@extends('app')
<head>
<script type="text/javascript" src="/ckeditor/ckeditor/ckeditor.js"></script>
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">

</head>

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<ul class="nav nav-tabs">
  
    <li role="presentation" class="dropdown">
    <a class="dropdown-toggle" href="{{ url('/post/new') }}" role="button" aria-expanded="false" >
      New Post 
    </a>
  </li>

  <li role="presentation" class="dropdown">
    <a class="dropdown-toggle" href="{{ url('/post/list') }}" role="button" aria-expanded="false" >
      Posts 
    </a>
  </li>
   
  <li role="presentation" class="dropdown">
    <a class="dropdown-toggle" href="{{ url('/home') }}" role="button" aria-expanded="false" >
      View Blog
    </a>
  </li>

</ul>


				<div class="panel-body">
					<h2>Edit Post</h2>
					<hr>
					<br>

          <?php

          $row=DB::table('posts')->where('id',$id)->first();
          $images=$row->images;
          $id=$row->id;
          ?>

					

						<form class="form-horizontal" role="form" method="POST" action="<?= URL::to('/post/update')?>" >
						    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  							<div class="form-group">
    							<label for="inputEmail3" class="col-sm-2 control-label"><b>Title</b></label>
   								 <div class="col-sm-6">
      								<input type="text" class="form-control" id="inputEmail3" placeholder="Title" name="title" value="<?= $row->title ?>">
    							</div>
  							</div>
                  <input type="hidden" class="form-control"  name="id" value="<?= $row->id ?>" >
                  <input type="hidden" class="form-control"  name="user_id" value=" {{ Auth::user()->id }} " >
                  <input type="hidden" class="form-control"  name="created_at" value="{{date('Y-m-d H:i:s')}}">
  							
                <div class="form-group">
    							<label for="inputPassword3" class="col-sm-2 control-label"><b>Content</b></label>
    								<div class="col-sm-8">
     									 <textarea class="ckeditor" id="ckeditor" placeholder="Content" cols="50" rows="30" name="content" ><?= $row->content ?></textarea>
   									 </div>
  							</div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label"><b>Image</b></label>
                   <div class="col-sm-6">
                    <?php 
                    if ($images > 0) {

                    echo '<a href="/uploads/'.$images.'" target="_blank" >View</a> | <a href="/post/update/image/'.$id.'" target="_blank"> Edit </a>| <a href="/post/delete/image/'.$id.'">Remove </a> ';
                    
                      }
                      else
                      {

                    
                     echo '<a href="/post/update/image/'.$id.'" target="_blank"> Upload Image </a>';
                  } ?>


                  </div>
                </div>




                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"><b>Publish On</label>
                    <div class="col-sm-6">


                       <div id="datetimepicker" class="input-append date">
      <input type="text" name="published_on" value="<?= $row->published_on ?>" >
     
      <span class="add-on">
        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      </span>
    </div>
    <script type="text/javascript"
     src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js">
    </script> 
    <script type="text/javascript"
     src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    <script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
    </script>
    <script type="text/javascript">
      $('#datetimepicker').datetimepicker({
        
        format: 'yyyy-MM-dd hh:mm:ss',
        language: 'My'
      });
    </script>
 
   
                     </div>
                </div>




  							
  							<div class="form-group">
    							<div class="col-sm-offset-2 col-sm-10">
      								<button type="submit" class="btn btn-default">Save</button>
    							</div>
  							</div>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
