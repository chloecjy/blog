@extends('app')

<head>

  <script>
var myWindow;


function closeWin() {
    myWindow.close();
}
</script>

  </head>

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Upload Image</div>

        <?php

          $row=DB::table('posts')->where('id',$id)->first();
          
          $id=$row->id;
          ?>

				<div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="<?= URL::to('/post/update/img')?>" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" class="form-control"  name="id" value="<?= $row->id ?>" >
                  <input type="hidden" class="form-control"  name="published_on" value="<?= $row->published_on ?>" >


              <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label"><b>Image</b></label>
                   <div class="col-sm-6">
                      <input type="file" class="form-control" id="inputEmail3" placeholder="Title" name="images" >
                  </div>
              </div>
              <br><br>

              <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-10">
                     <button type="submit" class="btn btn-default">Save</button> 
              </div>
          </form>
				
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
