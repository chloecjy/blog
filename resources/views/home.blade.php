@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>


       <?php if (Auth::check() && Auth::user()->name !=='Admin')
{
  ?>
    


				<ul class="nav nav-tabs">
  
  	<li role="presentation" class="dropdown">
    <a class="dropdown-toggle" href="{{ url('/post/new') }}" role="button" aria-expanded="false" >
      New Post 
    </a>
	</li>

	<li role="presentation" class="dropdown">
    <a class="dropdown-toggle" href="{{ url('/post/list') }}" role="button" aria-expanded="false" >
      Posts 
    </a>
	</li>
   
  <li role="presentation" class="dropdown">
    <a class="dropdown-toggle" href="{{ url('/home') }}" role="button" aria-expanded="false" >
      View Blog
    </a>
  </li>

</ul>
<?php  }

 if (Auth::check() && Auth::user()->name ==='Admin'){?>

 @include('admin_master')


<?php } ?>




				<div class="panel-body">
					<?php

        				
        				//$name=Auth::user()->name;
              $current=date('Y-m-d H:i:s');

        				$Post=DB::table('posts')->where ('published_on', '<=', $current)-> orderBy('published_on', 'desc')->paginate(10);

    					//$Post=DB::table('posts')->get();
   						 foreach($Post as $row){

                $uid=$row->user_id;
                $images=$row->images;
              
                $User=DB::table('users')->where ('id', $uid)-> get();

                foreach($User as $row2){

                  $username=$row2->name;
                }

    					?>


					<blockquote>
  						<h2><?php echo $row->title ?></h2>
  							<footer>Post On <b><?php echo $row->published_on ?></b> by <?php echo $username ?> </footer>
					</blockquote>
          
          <p> <?php echo $row->content ?></p>
          <br><br>
          <?php 
          if ($images > 0) {

            echo '<img src="uploads/'.$images.'"  style="width:304px;height:228px"/><br>';
          }
          ?>
      
          <hr>

        

					<?php } ?>


				</div>
       <?php echo $Post->render(); ?>
			</div>
		</div>
	</div>
</div>

@endsection
