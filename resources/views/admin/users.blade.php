@extends('app')


@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-13 col-md-offset-0">
      <div class="panel panel-default">
        <div class="panel-heading">Home</div>

        @include('admin_master')


        <div class="panel-body">
          <h2>Users</h2>
          <hr>
          <br>

          <table class="table">
     
      <thead>
        <tr>
         
          <th>Name</th>
          <th>Email</th>
          <th>Password</th>
          <th>Created On</th>
          <th>Updated On</th>
          <th></th>
        </tr>
      </thead>
      <tbody>

        <?php

        $id=Auth::user()->id;



        $Users=DB::table('users')-> orderBy('created_at', 'desc')->paginate(20);
    foreach($Users as $row){
    ?>

        <tr>
          
          <td><?php echo $row->name ?></td>
          <td><?php echo $row->email ?></td>
          <td ><?php echo $row->password ?></td>
           <td><?php echo $row->created_at ?></td>
            <td><?php echo $row->updated_at ?></td>
          <td><a href="<?= URL::to('admin/user/edit',array($row->id)) ?>"> Edit</a> | <a href="<?= URL::to('admin/user/delete',array($row->id)) ?>">Delete</td>
        </tr>
      <?php } ?>
        
      </tbody>
    </table>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection
