@extends('app')
<head>
<script type="text/javascript" src="/ckeditor/ckeditor/ckeditor.js"></script>
</head>

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				@include('admin_master')


				<div class="panel-body">
					<h2>Posts</h2>
					<hr>
					<br>

					<table class="table">
     
      <thead>
        <tr>
         
          <th>Title</th>
          <th>Created On</th>
          <th>Publish On</th>
          <th>Author</th>
          <th></th>
        </tr>
      </thead>
      <tbody>

        <?php

        $id=Auth::user()->id;

        $Post=DB::table('posts')->get();

        
        
    //$Post=DB::table('posts')->get();
    foreach($Post as $row){
    ?>

        <tr>
          
          <td><?php echo $row->title ?></td>
          <td><?php echo $row->created_at ?></td>
          <td><?php echo $row->published_on ?></td>

          <td><?php echo  $row->user_id ?></td>
          <td><a href="<?= URL::to('post/edit',array($row->id)) ?>"> Edit</a> | <a href="<?= URL::to('post/delete',array($row->id)) ?>">Delete</td>

        </tr>
      <?php } ?>
        
      </tbody>
    </table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
